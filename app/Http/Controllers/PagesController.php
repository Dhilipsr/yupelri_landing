<?php

namespace App\Http\Controllers;
use App\User;
use DB;
use App\Http\Resources\UserHealthToolCollection;
use App\Organizationtype;
use App\HomePage;
use App\Notifications\pluginEnquiry;
use App\Plugin;
use App\PluginOrder;
use Illuminate\Http\Request;
use Notification;
use Response;
use Mail;


class PagesController extends Controller 
{
    public function index()
    {   
        $content = HomePage::first();
        $pluginContent = Plugin::first();
        return view('welcome')->with(compact([
            'content',
            'pluginContent'
        ]));
    }



    public function pluginFormSubmit(Request $request)
    {
    // print_r("dfsdfsdfdsf");
//     // print_r($request);
    // exit;
//   if(strlen($token) > 0)
//   {

        $form = new PluginOrder;
        $quick_support='';
        $order_set_kit='';
        if ($request->quick_support) {
            $quick_support = 'yes';
        }else{
            $quick_support = 'no';
        }

        if ($request->order_set_kit) {
            $order_set_kit = 'yes';
        }else{
            $order_set_kit = 'no';
        }
        $form->quick_support = $quick_support;
        $form->order_set_kit = $order_set_kit;
        $form->name = $request->name;
        $form->email = $request->email;
        $form->professional_title = $request->professional_title;
        $form->name_of_organization = $request->name_of_organization;
        $form->organization_city = $request->organization_city;
        $form->organization_state = $request->organization_state;
         $form->manager_name = $request->manager_name;
        $form->type_of_organization = $request->type_of_organization;
        
        // print_r($form);
        // exit;
        $save = $form->save();
        
        if ($save) {
            // Notification::route('mail', $form->email)->notify(new pluginEnquiry($form));
            $formData = [
                'quick_support' => $quick_support,
           'order_set_kit' => $order_set_kit,
           'name' => $request->name,
           'email' => $request->email,
           'professional_title' => $request->professional_title,
           'name_of_organization' => $request->name_of_organization,
           'organization_city' => $request->organization_city,
           'organization_state' => $request->organization_state,
           'type_of_organization' => $request->type_of_organization,
                ]; 
            //  $email = 'support@scigqsasupport.com';
                $email = 'plugin@yopmail.com';
    //      $email = 'sanat.leo9@gmail.com';
            Mail::send('admin_notify',["data1"=>$formData],function($message) use ($email){
                $message->subject('Plugin Enquiry');
                $message->from('application@hasotech.com','Quick Support Access');
                $message->sender('application@hasotech.com', 'Quick Support Access');
                $message->cc('TEST@YOPMAIL.com');
                
                $message->to($email);
            });
            
            return response()->json(config('response.generic_success'));
        }else{
            return response()->json(config('response.generic_server_error'));
        }
    }
//     else
//     {
//          return response()->json(config('response.generic_server_error'));
//     }
// }
    public function samplePlugin()
    {
        return view('sample.plugin');
    }
    
     public function viewPdf()
    {
        return response()->file(public_path('yupelri_Overview_Flash_Card.pdf'));
    }
    
     public function pi()
    {
        return response()->file(public_path('yupelri 8.5 X 11 PI.pdf'));
    }
    
    public function getDownload(){
        
        return redirect(asset('order_set_kit.zip'));
    }
    
    
    public function getUser(Request $request)
    {
        // print_r("check");
        // die;
        if($request->token == 'PwK6En[9')
        {
            //  echo $request->token;
            // exit;
            // $users = User::where('id', '!=' , 1)->get();
            $users= DB::table('plugin_orders')->where('id', '!=' , 1)->get();
            // print_r($users);
            // exit;
            return UserHealthToolCollection::collection($users);
        }
        else
        {
            console.log('error');
        }
    }
    
    public function getOrder(Request $request)
    {
        // print_r($request->token);
        // exit;
        if( $request->token == 'j!yt!ocb!im)' )
        {
            $orders = PluginOrder::get();
            return response()->json(['data' => $orders]);
        }
        else
        {
            return response()->json(['status' => 401 , 'message' => 'Unauthenticated']);
        }
    }
    
}
