<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Role;

use DB;
class UserHealthToolCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        
      
       // return $request->All();

        // $organization = Organizationtype::where('id',$this->organization_id)->first();
        // return $organization;
        return 
        [
            'user_name' => $this->name,
            'quick_support'      =>$this->quick_support,
            'order_set_kit'  =>$this->order_set_kit,
            'email'         => $this->email,
           'name_of_organization' =>$this->name_of_organization,
           'organization_state'  =>$this->organization_state,
           'organization_city'  =>$this->organization_city,
           'manager_name'      =>$this->manager_name,
           'type_of_organization'      =>$this->type_of_organization,
           'manager_name'      =>$this->manager_name,
           'updated_at'      =>$this->updated_at,
           'created_at'      =>$this->created_at,
        ];
    }

// public function Name($id){
//     $null='';
//      $data= DB::table('salix_managers')->where('id',$id)->get();
   
//     // print_r($data[0]->salix_manager_name);exit;
//      if(count($data) !=0){
//       return $data[0]->salix_manager_name;
//      }else{
//          return $null;
//      }
//     }
}