 <script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div id="myModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-box">
        <div class="default-box">
            <div class="modal-header">

                <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="heading-text" style="color:#005EB8;">YUPELRI<sup>&reg;</sup> Quick Support & Access Order Form
                            </h4>
                            <p class="st-ee">Simple steps to order:</p>
                            <ul class="model-list">
                                <li><span>1)</span> Complete the registration information below and hit Submit.</li>
                                <li><span>2)</span> A member of the QSA team will contact you to confirm your order.</li>
                                <li><span>3)</span> Your order will be sent to you in 1 week (5 business days).
                                </li>
                            </ul>
                            <p>If you have any questions, please contact <a class="em1" 
                        href="mailto:support@yupelrisupport.com"
                                    target="_black">support@yupelrisupport.com.</a></p>

                        </div>
                    </div>
                </div>
                <div class="mainBody sec-bt" style="">
                    <section class="container">
                        <div class="row">
                            <div class="col-12">


                                <p class="form-title">Please select which item(s) you are interested in:</p>
                                <form class="form-validate" id="email-form">


                                    <label class="container-checkbox">YUPELRI Quick Support & Access Plugin
                                        <input type="checkbox" id="quick_support" name="quick_support" value="quick_support">
                                        <span class="checkmark"></span>
                                    </label>
                                   

                                    <div class="noted">
                                        <label><span>*</span>Indicates required field</label>

                                    </div>


                                    <div class="form-group req">
                                        <p>Your name</p>
                                        <input id="name" type="text" class="form-control"  name="name" required>
                                        {{-- <div class="invalid-feedback">Please enter your name</div> --}}
                                        <b id="name_error"></b>
                                    </div>
                                    <div class="form-group req">
                                        <p>Your email</p>
                                        <input id="email" type="email" name="email" class="form-control" required>
                                        {{-- <div class="invalid-feedback">Please enter your email</div> --}}
                                        <b id="email_error"></b>
                                    </div>

                                    <div class="form-group req">
                                        <p>Your professional title</p>
                                        <input id="professional_title" type="text" name="professional" class="form-control" required>
                                        {{-- <div class="invalid-feedback">Please enter your professional title</div> --}}
                                        <b id="professional_title_error"></b>
                                    </div>

                                    <div class="form-group req">
                                        <p>The name of your organization</p>
                                        <input id="name_of_organization" type="text" name="organization" class="form-control" required>
                                        {{-- <div class="invalid-feedback">Please enter your organization's name
                                        </div> --}}
                                         <b id="name_of_organization_error"></b>
                                    </div>

                                    <div class="form-group req">
                                        <p>Your organization’s city</p>
                                        <input id="organization_city" type="text" name="city" class="form-control" required>
                                        {{-- <div class="invalid-feedback">Please enter your organization's city
                                        </div> --}}
                                        <b id="organization_city_error"></b>
                                    </div>

                                    <div class="form-group req">
                                        <p>Your organization’s state</p>
                                        <input id="organization_state" type="text" name="state" class="form-control" required>
                                        {{-- <div class="invalid-feedback">Please enter your organization’s state
                                        </div> --}}
                                        <b id="organization_state_error"></b>
                                    </div>
                                    <div class="form-group">
                                        <label>Your YUPELRI Account Manager’s name (optional)</label>
                                        <input id="manager_name" name="manager_name" type="text" class="form-control" >
                                        {{-- <div class="invalid-feedback">Please enter your organization’s state </div> --}}
                                    </div>
                                    <div class="form-group req mb-2">
                                        <p>Type of organization</p>
                                    </div>
                                    <label class="container-checkbox">Large Group Practice or Physician Group

                                        <input type="radio" class="type_of_organization" value="Large Group Practice or Physician Group" name="type_of_organization">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="container-checkbox">Health System
                                        <input type="radio" class="type_of_organization" value="Health System" name="type_of_organization">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="container-checkbox" style="">

                                        Other:
                                        <input type="radio" value="other" name="type_of_organization">
                                        <span class="checkmark"></span>
                                        <input type="text" id="other" class="other type_of_organization">


                                    </label>
                                    <b id="type_of_organization_state_error"></b>



                                    <div class="form-action   ">
                                        <label class="container-checkbox">I hereby certify I am a healthcare
                                            provider.<sup style="color:red">*</sup>
                                            <input type="checkbox" id="tandc" name="tnc">
                                            <span class="checkmark"></span>
                                            <div class="csl"><p class="las" >Viatris respects your privacy. For an explanation 
of how Viatris will use the information you are 
submitting, please view our <a target="_blank" href="https://www.viatris.com/en/privacy-policy">Privacy Policy</a>.</p></div>
                                        </label>
                                      <div class="form-action mb-2" id="recap" style='padding-bottom:em';>
                                         
                                            <div class="g-recaptcha" data-sitekey="6LdXhgoeAAAAACi_-kFzicaqSV9SY6Z6T_Xb7nIG" data-callback="verifyCaptcha" onclick="myFunction()"></div>
                                             <div id="demo"></div>
                                          </div>
                                          
                                         
                                        <b id="tandc_error"></b>
                                       <div class="form-action ids">
                                            <button type="submit" class="model-btn" id="send_email" style="margin-bottom:2em;">Submit</button>
                                        </div>
                                        
                                      
                                       
                                    </div>
                                             <div class="form-action ids">
                                      <button class="gh" onclick="verify()">Submit</button>
                                      
                                </div>
                                </form>
                                
                               
                              
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
  lable.remove {
    background: #ffffff;
    padding-right: 72px;
    padding-left: 77px;
    padding-top: 13px;
    padding-bottom: 14px;
    color: #64a944;
    font-size: 20px;
    font-weight: 600;
    border-radius: 42px;
    margin-left: 9em;
}
.adj1 {
    padding-left: 24px;
    /*padding-top:3em;*/
    padding-bottom: 2em;
}

button.gh {
    margin-bottom: 6em;
    /*margin-left: 19em;*/
    background: #ffffff;
    padding-right: 72px;
    padding-left: 77px;
    padding-top: 13px;
    padding-bottom: 14px;
    color: #64a944;
    font-size: 20px;
    font-weight: 600;
    border-radius: 42px;
    margin-bottom: 36px;
    /*margin-left: 448px;*/
    margin-top:0.9em;
    border: solid #085cbc;
}
div#demo {
    padding-left: 3em;
    padding-top: 1em;
    color: #dc3545;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
    async defer>
</script>

<script>
    function verify()
    {
      document.getElementById("demo").innerHTML = "The reCAPTCHA wasn't entered correctly!";
    }
</script>
<script type="text/javascript">
$('button#send_email').removeAttr('id');
$('.model-btn').hide();

  function verifyCaptcha() { 
      $("button").attr("id", "send_email");
      $('.gh').hide();
      $('#demo').hide();
      $('.model-btn').show();
    
  };
   
</script>



<div id="myModal-2" class="modal hide fade" tabindex="-1" data-replace="true">
    <div class="modal-dialog modal-box ">
        <div class="default-box">
            <div class="modal-body">
                <button type="button" onclick="myFunction123()" class="model-close close xhN" data-dismiss="modal" aria-label="Close" id=clr-cae>
                    ×
                <div class="mainBody sec-btmm">
                    
                </button>
                    <section class="container">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h4 class="headtxt">YUPELRI<sup>&reg;</sup> Quick Support & Access Order Form
                                </h4>

                                <p class="sm" style="color: white;"><strong>Thank you for ordering YUPELRI Quick Support & Access!</strong><br>
                                   You should expect an email within the next 48 hours with <br>
                                    more information and next steps.
                                </p>


                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>


