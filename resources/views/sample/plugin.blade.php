<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Yupelri – QSA – SAMPLE PLUGIN</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="Yupelri_favicon.png">
    <link rel="icon" href="{{asset('Yupelri_favicon.png')}}">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="{{asset('sample/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('sample/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/sampleMain.css')}}">

</head>

<body>
   

    <!-- Header Starts -->

    <div class="frame ">
        <div class="samp-img"><i>sample</i></div>
        {{-- <div class="manu-sec">
            <div class="mob-menu-box">

                <div class="menu-icon"><span></span>
                    <p>menu</p>
                </div>
                <div class="mamber-logo"><img src="{{asset('img/man-logo.png')}}"></div>
            </div>
        </div>
        <div class="mob-menu-up">
            <div class="mob-menu">
                <div class="mob-close-box"><span class="mob-close"><img src="img/close-white.png" alt="img"></span>
                </div>
                <ul class="navigation">
                    <li><a href="login.html">Log In</a></li>

                </ul>
            </div>
        </div> --}}
        <header class="">
            <div class="side-space">
                <section class="container">
                    <div class="row">
                        <div class="col-12  ">
                            <div class="bgr">
                                <div class="logo" style="padding"><img src="{{asset('sample/img/YUPELRI_QSA_logo.png')}}">
                                </div>
                              
                                <div class="indication">



                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </header>
      
        <!-- Header Ends -->



        <!-- Main Section -->
        

       <form class="form">
            <section class="side-space main-section">
                <section class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="top-link ">
                                <ul>
                                    <li class=""><a href="#provider">Provider</a></li>
                                   
                                    <li class=""><a href="#patients"> Patient</a></li>
                                     
                              
                                    <li><a href="https://dailymed.nlm.nih.gov/dailymed/fda/fdaDrugXsl.cfm?type=display&setid=6dfebf04-7c90-436a-9b16-750d3c1ee0a6"
                                            target="_blank">Prescribing Information</a></li>
                                            
                                    <li><a href="#isi" data-scroll> Important Safety Information</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-12">
                            <nav class="notification">
                                <ul>
                                    <li><img src="{{asset('sample/img/select.png')}}"> Select tool(s)</li>
                                    <li><img src="{{asset('sample/img/search.png')}}"> Preview tool</li>
                                    <li><img src="{{asset('img/arrow.png')}}"> Description</li>

                                </ul>
                            </nav>
                        </div>
                    </div>

                    <div class="row headings " id="provider">
                        <div class="col-6">
                            <div class="mr-left fsdf">
                                <span >
                                <h3 class="pro">PROVIDER</h3>
                            </span>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-12  mr-left">
                          
                        </div>
                    </div>


                    
                        
                        
                    <div class="row ">
                        <div class="col-12 ">
                            <div class="pd-left">
                                <div class="acc pd-left">
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="01"
                                                    data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                                    data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                                    data-title="Considerations for PAs">
                                            
                                              <label class="dfdd" for="01"></label> <div class="adn" >YUPELRI Coverage Information</div></label>
                                                     
                                            </span>
                                            <a href="https://cc-oge.online/thb-yupelri/yupelri-coverage-information-sample-7294"
                                        
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>
                                        <div class="acc__panel  mr-left">
                                          
                                           Information regarding Medicare, commercial, and uninsured patient coverage for YUPELRI. Also includes prescription examples and J-CODE information.
                                        </div>
                                    </div>
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="02"
                                                    data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                                    data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                                    data-title="Counseling Tips">
                                                   
                                                
                                                 
                                            </span> 
                                         
                                        </div>

                                        <div class="acc__panel  mr-left">
                                          
                                          
                                            Offers clinical management troubleshooting techniques to possibly help resolve potential 
                                           infusion issues that may arise
                                        </div>
                                    </div>
                                    
                                    
                                   
                                       <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="03"
                                                    data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                                    data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                                    data-title="Counseling Tips">
                                                    
                                                    
                                     
                                            </span>
                                          
                                        </div>

                                       <div class="acc__panel  mr-left">
                                        
                                        Helps patients get started on yupelri through the yupelri Connect resource center
                                        </div>
                                    
                                    </div>
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="04"
                                                    data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                                    data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                                    data-title="Hepatic Encephalopathy Hospital Discharge Tips">
                                           
                                            </span>
                                           
                                        </div>

                                        <div class="acc__panel  mr-left">
                                         
                                            Helps eligible patients receive a trial of yupelri
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                         <section class="container bg-grey">
                         <div class="education-grey">
                           Educational resources for patients</span>
                        </div>
                         </section>


                     <div class="row headings " id="patients">
                        <div class="col-6">
                            <div class="mr-left jjg">
                                <h3 class="pro">PATIENT</h3>
                            </div>
                        </div>
                      
                    </div>


               
                    <div class="row ">
                        <div class="col-12 ">
                            <div class="pd-left">
                                <div class="acc pd-left">
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="05">
                                                <label for="05">How to Use YUPELRI Video</label>
                                            </span>
                                            <a href="https://cc-cms.online/cmsbuilder/page_preview/dist/e97bc0715029f5940045f6619db22d36"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>
                                        <div class="acc__panel  mr-left">
                                        
                                             <div>Video providing detailed instructions on how to administer YUPELRI
                                             </div></span><br>
                                            
                                           
                                        </div>
                                    </div>
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="06">
                                               
                                                
                                                
                                                  <label class="dfdd" for="06"></label> <div class="adn" >YUPELRI Instructions for Use and Cleaning</div></label>
                                                     
                                            </span>
                                            <a href="https://cc-oge.online/thb-yupelri/yupelri-instructions-for-use-and-cleaning-sample-7296"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>

                                        <div class="acc__panel  mr-left ">
                                          
                                            <div>
                                                This patient information booklet contains YUPELRI’s Instructions for Use, as well as general guidance on how to clean a nebulizer.
                                            </div>
                                            <br>
                                            
                                        </div>
                                    </div>
                                        <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="09">
                                               
                                                
                                                
                                                  <label class="dfdd" for="09"></label> <div class="adn" >YUPELRI Patient Coverage Information</div></label>
                                                     
                                            </span>
                                            <a href="https://cc-oge.online/thb-yupelri/yupelri-patient-coverage-information-sample-7300"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>

                                        <div class="acc__panel  mr-left ">
                                          
                                           <div>
                                               Information for patients regarding YUPELRI’s Medicare and commercial insurance coverage
                                           </div>
                                                                                       <br>
                                            
                                        </div>
                                    </div>
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="07">
                                                <label for="07">YUfirst Flyer</label>
                                            </span>
                                            <a href="https://cc-oge.online/thb-yupelri/yufirst-flyer-sample-7292"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>

                                        <div class="acc__panel  mr-left">
                                            <!-- Use your free login to submit ePA form. -->
                                           <div class="clera">Learn more about YUPELRI’s patient resource program that offers information and content to promote patients taking an active role in their treatment with YUPELRI.</div>
                                             <div class="cobox" style="padding-top:10px;">Content contained in the Quick Support and Access plugin is being provided by Viatris Inc. for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</div>
                                          
                                        </div>
                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>

                </section>
            </section>


        
                    <div id="tab-2" class="tab-content ">
                        <div class="row headings ">
                            <div class="col-md-8">
                                <div class="mr-left">
                                    <h3>Caregiver and Patient 02</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12  mr-left">
                                <div class="pd-left sub-title">
                                    <h3>Resources 02</h3>
                                </div>
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-12 ">
                                <div class="pd-left">
                                    <div class="acc pd-left">
                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="selector[]" class="checkbox" id="07">
                                                    <label for="07">yupelri Instant Savings Card

                                                        <small class="small-info">PLEASE NOTE: This is an online form
                                                            that
                                                            allows you to download the card. It is not available
                                                            in Spanish.</small>
                                                    </label>

                                                </span>
                                                <a class="preview"><img src="{{asset('sample/img/search.png')}}"></a> <a
                                                    class="drop-down"><img src="img/arrow.png"></a>
                                            </div>
                                            <div class="acc__panel  mr-left">
                                                Provides most eligible, commercially insured patients help with their
                                                monthly
                                                co-pays for yupelri. <span class="note">PLEASE NOTE: This tool can not
                                                    be
                                                    printed.</span>
                                            </div>
                                        </div>
                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="selector[]" class="checkbox" id="08">
                                                    <label for="08">Reduce the Risk of OHE Recurrence With
                                                        yupelri</label>
                                                </span>
                                                <a class="preview"><img src="{{asset('sample/img/search.png')}}"></a> <a
                                                    class="drop-down"><img src="img/arrow.png"></a>
                                            </div>

                                            <div class="acc__panel  mr-left">
                                                Provides information about yupelri coverage, prescription savings and
                                                how to
                                                sign up
                                                for the HE Living Program (H.E.L.P.).

                                            </div>
                                        </div>

                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="selector[]" class="checkbox" id="09">
                                                    <label for="09">Setting up Medical Alerts on Digital Devices</label>
                                                </span>
                                                <a class="preview"><img src="{{asset('sample/img/search.png')}}"></a> <a
                                                    class="drop-down"><img src="img/arrow.png"></a>
                                            </div>

                                            <div class="acc__panel  mr-left">
                                                Instructions for setting up alerts on an iPhone, android or Apple Watch.
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                       
                    </div>




                <section class="container">
                    <a class="back-to-top" >
                        <span style="color:#8c8b8b";>Back to Top</span>
                         <img src="img/ge.png" class="bt">
                    </a>
                    <!--   <hr class="gr-divi"> -->
                </section>
                </section>
                <!-- BG-BLUE added by me down -->
                <section class="BG-BLUE">
                


                <section class="container">
                    <div class="row ">
                        <div class="col-12 d-flex justify-content-center action">
                            <div href="#" data-toggle="modal" data-target="#myModal" class="button id">Email</div>
                            <div class="button" data-toggle="modal" data-target="#print">View/Print
                            </div>
                            <div class="button" data-toggle="modal" data-target="#copy" id="copy-tool">Copy Link</div>
                        </div>
                    </div>
                </section>
                    </section>

                <section class="para">
                    <div class="row ">
                        <div class="col-12 imp-safety">
                            <h6 class="head" id="isii">Indication</h6>
                            
                            <div class="yup">
                                YUPELRI® inhalation solution is indicated for the maintenance treatment of patients with chronic obstructive pulmonary disease (COPD).
                            </div>
                            <h6 class="head" id="isi">Important Safety Information</h6>
                           <div class="yup">
                               YUPELRI is contraindicated in patients with hypersensitivity to revefenacin or any component of this product.
                           </div>
                           
                            <div class="yup" style=padding-top:10px;>YUPELRI should not be initiated in patients during acutely deteriorating or potentially life-threatening episodes of COPD, or for the relief of acute symptoms, i.e., as rescue therapy for the treatment of acute episodes of bronchospasm. Acute symptoms should be treated with an inhaled short-acting beta<sub>2</sub> -agonist.</div>
                                
                             <div class="yup" style=padding-top:10px;>As with other inhaled medicines, YUPELRI can produce paradoxical bronchospasm that may be life-threatening. If paradoxical bronchospasm occurs following dosing with YUPELRI, it should be treated immediately with an inhaled, short-acting bronchodilator. YUPELRI should be discontinued immediately and alternative therapy should be instituted.</div>
                                     
                             <div class="yup" style=padding-top:10px;>YUPELRI should be used with caution in patients with narrow-angle glaucoma. Patients should be instructed to immediately consult their healthcare provider if they develop any signs and symptoms of acute narrow-angle glaucoma, including eye pain or discomfort, blurred vision, visual halos or colored images in association with red eyes from conjunctival congestion and corneal edema.</div>
                                          
                             <div class="yup"style=padding-top:10px;>Worsening of urinary retention may occur. Use with caution in patients with prostatic hyperplasia or bladder-neck obstruction and instruct patients to contact a healthcare provider immediately if symptoms occur.</div>
                                               
                             <div class="yup" style=padding-top:10px;>Immediate hypersensitivity reactions may occur after administration of YUPELRI. If a reaction occurs, YUPELRI should be stopped at once and alternative treatments considered.</div>
                                                    
                            <div class="yup" style=padding-top:10px;>The most common adverse reactions occurring in clinical trials at an incidence greater than or equal to 2% in the YUPELRI group, and higher than placebo, included cough, nasopharyngitis, upper respiratory infection, headache and back pain.</div>
                                                         
                             <div class="yup" style=padding-top:10px;>Coadministration of anticholinergic medicines or OATP1B1 and OATP1B3 inhibitors with YUPELRI is not recommended.</div>
                                                              
                             <div class="yup"></div>

<div class="yup" style=padding-top:10px;>Please see full <a class="yup" target="_blank" href="https://dailymed.nlm.nih.gov/dailymed/fda/fdaDrugXsl.cfm?type=display&setid=6dfebf04-7c90-436a-9b16-750d3c1ee0a6">Prescribing Information</a> For additional information, please contact us at <span style="color:#007bff;">1-800-395-3376 </span>.</div>

      



<div class="logo123">
    <img class="min" src="{{asset('img/FooterLogo.png')}}">
    <div class="con">
     Questions?<a class="con1" href="http://go.aventriahealth.com/THBYupelriContactForm.html" target="_blank">Contact Us</a>
</div>
</div>



<div class="fobox">
    
<div class="foo">
   YUPELRI and the Yupelri Logo are registered trademarks of Mylan Specialty L.P., a Viatris Company. <br>
VIATRIS and the Viatris Logo are trademarks of Mylan Inc., a Viatris Company.<br>
THERAVANCE BIOPHARMA®, THERAVANCE®, and the Cross/Star logo are registered trademarks of the Theravance Biopharma group of companies (in the U.S. and certain other countries).<br>
<span>PARI TREK and PARI LC SPRINT are registered trademarks of PARI GmbH.</span><br>

</div>
</div>
<div class="foo" style="padding-top:27px">© 2021 Viatris Inc. All Rights Reserved. REV-2021-0328</div>

<div class="foo" style="padding-top:27px; "><a style="text-decoration: none;" class="fr_fd" target="_blank" href="https://www.viatris.com/en/privacy-policy">Privacy Notice  </a>| <a target="_blank" style="text-decoration: none;" class="fr_fd" href="https://www.viatris.com/en/Terms-of-Use">Copyright and Legal Disclaimer </a></div>


                        </div>
                    </div>
                </section>
            </section>


        </form>
        <!-- Main Section Over-->

        <div class="copy-cbd  alert-success">Copy to Clipboard</div>
        <div class="uncopy-cbd alert-danger">Checkbox is not selected</div>


      
    </div>

    <!-- Modal Starts here-->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <div class="default-box  modal-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to email the selected resource link
                        to a patient.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
               
            </div>

        </div>
    </div>
    <!-- Modal Ends Here -->

    <!-- Modal Starts here-->
    <div id="print" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <div class="default-box  modal-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to print the selected tool.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

            </div>

        </div>
    </div>
    <!-- Modal Ends Here -->

    <!-- Modal Starts here-->
    <div id="copy" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <div class="default-box  modal-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to copy the link for the selected
                        resource to email to a
                        patient.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

            </div>

        </div>
    </div>
    <!-- Modal Ends Here -->




    <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/sample/plugins.js')}}"></script>
    <script src="{{asset('js/sample/main.js')}}"></script>

</body>

</html>