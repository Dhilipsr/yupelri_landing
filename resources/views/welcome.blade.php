@extends('layout.layout')

@section('title')
 yupelri QSA - Quick Support and Access
@endsection

@section('css')
 <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
@endsection

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="mainBody">
        <div class="grey-bac" >
            <section class="container">
                <h6 class="main-topline">
                 {!! $content->section_one_content !!}
                 
                </h6>
                
                <div  class="quick-support" >
                    {!! $content->section_two_content !!}
                      
                </div>
             </section>
        </div>
        <hr style="width: 90%; border: 1px solid #d3d3d3;">
        <section class="container" id="ind2">
            <div class="row">

            
                <div class="col-12">
                  
             <div class="col-12">
             
          </div>
                </div>

               <div class="col-12">
         

                <div class="col-12">
                    <div class="content">
                        <span class="con-tw">

                      
                    </span>
                    </div>
                    <div class="imp-content">

                        <h6 class="ind">Indications</h6>
                        <p>YUPELRI<sup>&reg;</sup> inhalation solution is indicated for the maintenance treatment of patients with chronic obstructive 
                         pulmonary disease (COPD).</p>
                            
                         <h6 class="" id="isi">Important Safety Information</h6>
                       <!--<div class="saf"></div>-->

                            <p>YUPELRI is contraindicated in patients with hypersensitivity to revefenacin or any component of this product.</p>
                       
                      
                       <p>YUPELRI should not be initiated in patients during acutely deteriorating or potentially life-threatening episodes 
of COPD, or for the relief of acute symptoms, i.e., as rescue therapy for the treatment of acute episodes of 
bronchospasm. Acute symptoms should be treated with an inhaled short-acting beta<sub>2</sub>
-agonist.</p>
                           <p>As with other inhaled medicines, YUPELRI can produce paradoxical bronchospasm that may be life-threatening. 
If paradoxical bronchospasm occurs following dosing with YUPELRI, it should be treated immediately with an 
inhaled, short-acting bronchodilator. YUPELRI should be discontinued immediately and alternative therapy 
should be instituted.</p>

                        <p>
                            YUPELRI should be used with caution in patients with narrow-angle glaucoma. Patients should be instructed to 
immediately consult their healthcare provider if they develop any signs and symptoms of acute narrow-angle 
glaucoma, including eye pain or discomfort, blurred vision, visual halos or colored images in association with 
red eyes from conjunctival congestion and corneal edema.
                        </p>
                           
                        
                        <p class="add">Please see additional Important Safety Information <a class="bel" data-scroll href="#note">below</a> and <a class="cli" href="https://dailymed.nlm.nih.gov/dailymed/fda/fdaDrugXsl.cfm?type=display&setid=6dfebf04-7c90-436a-9b16-750d3c1ee0a6" target="_blank">click
                                here</a> for full Prescribing Information.</p>
                    </div>

                    {{-- <div class="imp-content">
                         {!! $content->section_three_title !!}
                    </div> --}}
                </div>
            </div>

        </section>

    </div>

    <div class="mainBody sec-btm">
        <section class="container">
            <div class="row flex-sm-row-reverse">

                <div class="col-md-6">
                    <div class="content">
                        <div class="con_one" style="">
                         {!! $pluginContent->content_one !!}
                     </div>
                     <?php  
                     
                $urllink = $pluginContent->Leave_behind;                 
                     
                     ?>
                         <ul class="buttons bdr">
                            <li><a href="#" class="btn d-blue gaClick" data-toggle="modal" data-target="#myModal" data-label="yupelri Support Order">Request Plugin</a></li>
                            <li><a href="https://www.research.net/r/yupelri-qsa" class="btn gaClick" target="_blank" data-label="GINA">Ask GINA&trade;</a></li>
                            
                        </ul>
                        {!! $pluginContent->content_two !!}
                        <ul class="buttons bdr">
                           
                         
                             <li><a href=" <?php echo $urllink ?>" target="_blank" class="btn gaClick" data-label="Flashcard">View Leave-behind</a>
                            </li>

                        </ul>

                        <p class="note">GINA is a trademark of Aventria Health Group.</p>
                        
                            <p class="note" style="color: #000000;">Content contained in the Quick Support and Access plugin is being provided by Viatris Inc. for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>
                    </div>
                </div>
                 <?php
            
            $pass=$pluginContent->image;
           
            ?>
                <div class="col-md-6">
                    <div class="img-sec">
    <a href="{{route('samplePlugin')}}"  target="_blank" class="gaClick" data-label="Sample"><img src="https://web.myyupelriqsa.com/storage/<?php echo $pass ?>" ></a>
                    </div>
           
                    <ul class="buttons bdr text-center" id="de">
                        <li><a href="{{route('samplePlugin')}}" target="_blank" class="btn gaClick" data-label="Sample">View Live Sample</a>
                        </li>

                    </ul>
                </div>
            </div>
        </section>

    </div>
    <div id="here"></div>
     <div class="mainBody" id="note">
        <section class="container"id="#imnote">
            <div class="row" >

                <div class="col-12">


                    <div class="imp-content" >
                        <h6 id="note1">Important Safety Information (Continued)</h6>


                        <p>Worsening of urinary retention may occur. Use with caution in patients with prostatic hyperplasia or bladder-neck 
obstruction and instruct patients to contact a healthcare provider immediately if symptoms occur.</p>

        <p>Immediate hypersensitivity reactions may occur after administration of YUPELRI. If a reaction occurs, YUPELRI 
should be stopped at once and alternative treatments considered.</p>
        <p>The most common adverse reactions occurring in clinical trials at an incidence greater than or equal to 2% in the 
YUPELRI group, and higher than placebo, included cough, nasopharyngitis, upper respiratory infection, headache 
and back pain.</p>

<p>
  Coadministration of anticholinergic medicines or OATP1B1 and OATP1B3 inhibitors with YUPELRI is not 
recommended.
</p>
<p>
    YUPELRI is not recommended in patients with any degree of hepatic impairment.
</p>

                       <!--  <h6>IMPORTANT SAFETY INFORMATION</h6> -->
                       <!--  <ul class="mb-4"

                        </ul> -->
                           <p>Please see full <a class="ing" href="https://dailymed.nlm.nih.gov/dailymed/fda/fdaDrugXsl.cfm?type=display&setid=6dfebf04-7c90-436a-9b16-750d3c1ee0a6" target="_blank">Prescribing Information</a> for additional information please contact us at <a href="tel:1-800-395-3376">1-800-395-3376</a>.</p>

                       
                     
                    </div>
                    
                
                
                </div>
            </div>

        </section>

    </div>
@endsection

@section('js')
  <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
   <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"></script> 
    

    <!--</script>-->
    
    
    
    
  
<script type="text/javascript">

$("#email-form").validate({
    rules:{
        type_of_organization:{
                required:true
            },
        tnc:{
            required:true
        },
          quick_support:{
            required:true
        }
        
        
    },
        messages: {
            name: "Please enter your name",
            email: "Please enter your email",
            professional: "Please enter your professional title",
            organization: "Please enter your organization's name",
            city: "Please enter your organization's city",
            state: "Please enter your organization’s state",
            type_of_organization:"Please select at least one type of organization",
            tnc:"Please certify you are a healthcare provider",
             quick_support:"Please select at least one checkbox"
             

        },
        // 'g-recaptcha-response' => 'required|captcha'
    });
    var quick_support; 
          $('#quick_support').on('change', function() {
                 quick_support = this.checked ? this.value : '';
                  
        //   console.log(quick_support);
                });
     
      var order_set_kit; 
          $('#order_set_kit').on('change', function() {
                 order_set_kit = this.checked ? this.value : '';
                  
        //   console.log(order_set_kit);
                });

    $(document).on('click','#send_email', function(e){
        // get the values
    //   function onloadCallback() {
      
        //   alert("dfdf");
        var name = $('#name').val();
        var email = $('#email').val();
        var professional_title = $('#professional_title').val();
        var name_of_organization = $('#name_of_organization').val();
        var organization_city = $('#organization_city').val();
        var organization_state = $('#organization_state').val();
         var manager_name = $('#manager_name').val();
        var type_of_organization = $("input[name='type_of_organization']:checked").val();
        if(type_of_organization == 'other')
        {
            type_of_organization = $("#other").val();
        }
        var tandc = $('#tandc').val();
        // console.log(quick_support,order_set_kit,name,email,professional_title,name_of_organization,organization_city,organization_state,type_of_organization);
       
      

        if($('#email-form').valid())
        {
           
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                } 
            });
            $.ajax({
              
                url : "{{route('plugin.form.submit')}}",
                type: "POST",
                data : {'quick_support' : quick_support, 'order_set_kit' : order_set_kit, 'name' : name, 'email' : email, 'professional_title' : professional_title, 'name_of_organization' : name_of_organization, 'organization_city' : organization_city, 'organization_state' : organization_state,'manager_name': manager_name, 'type_of_organization' : type_of_organization},
                success: function(response)
                {   
                    //  console.log(response)
                     if(response.status == 200) {
                   
                         $('#myModal').remove();   
                        // $('#myModal').removeClass('show');
                        $('#myModal-2').addClass('show');
                        $('#myModal-2').css('display','block');
                     }
                    // return (response)
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
             
                }
            });
        }
        
        e.preventDefault();
    //   }
    });
    


</script>


<script>
    
    function myFunction123()
    {
         location.reload();
    }
</script>
<script>
    function myFunction() {
        
  $('.close').click(function () {
            // $('.modal-backdrop').remove();
            // $(this).parents('#myModal-2').hide();
            location.reload();
        });
}
</script>


<script>
 
</script>
@endsection